package com.oreilly.cloud;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
@RestController
@EnableEurekaClient
@EnableHystrix
@EnableHystrixDashboard
public class SpringMicroservicesLibraryCatalogApplication {

  @Value("${catalog.size}")
  private int size;

  @RequestMapping("/catalog")
  @CrossOrigin
  @HystrixCommand(fallbackMethod = "fallback")
  public List<Book> getCatalog() {
    return Book.getBooks().subList(0, size);
  }

  public List<Book> fallback() {
    System.out.println("Inside fallback method - Jana!");
    return Book.getDefaultBooksList().subList(0, 2);
  }

  public static void main(String[] args) {
    SpringApplication.run(SpringMicroservicesLibraryCatalogApplication.class, args);
  }
}
